# Instal GitLab CE di Debian-_based_ GNU/Linux

## Instal Paket Depedensi

```sh
$ sudo apt update
$ sudo apt upgrade
$ sudo apt install curl openssh-server ca-certificates
```

## Tambahkan Repo GitLab dan Instal Paketnya

```sh
$ curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh | sudo bash
$ EXTERNAL_URL="http://gitlab.example.com" sudo apt install gitlab-ce
```
> Pada bagian **EXTERNAL_URL** silahkan ubah nilainya sesuai domain yang anda gunakan, jika anda menginstalnya di server lokal, ganti nilainya dengan IP server tersebut, misalnya: **EXTERNAL_URL="http://192.168.1.1"**.

Anda juga dapat menambahkan nilai **EXTERNAL_URL** nanti setelah proses instalasi GitLab selesai. Jika demikian, anda dapat mengkonfigurasinya pada `/etc/gitlab/gitlab.rb`, setelah itu, jalankan perintah `sudo gitlab-ctl reconfigure` agar konfigurasi anda dapat diterapkan.

## Uji Coba
Kini, anda sudah dapat mengakses GitLab dengan mengetikkan domain atau IP server anda pada kolom pencarian peramban web. Saat anda mengaksesnya pertama kali, anda akan diminta untuk mengubah sandi anda.


# Mengamankan GitLab dengan Let's Encrypt

Let's Encrypt adalah layanan yang menyediakan permintaan sertifikat SSL/TLS secara bebas. Sertifikat SSL yang di _generate_ oleh Certbot dapat anda gunakan selama 90 hari.

> Agar Let's Encrypt dapat digunakan, anda harus memiliki domain yang sudah terdaftar secara resmi. Anda dapat mengunjungi laman [Bagaimana Cara Kerja Let's Encrypt](https://letsencrypt.org/how-it-works/) untuk mengetahui informasi lebih lanjut.

## Instal Certbot, Let's Encrypt Client
```sh
$ sudo apt install certbot
```

## Mempersiapkan Let's Encrypt untuk Verifikasi Domain

```sh
$ sudo mkdir -p /var/www/letsencrypt
```

```sh
$ sudo nano /etc/gitlab/gitlab.rb
```

Carilah baris **nginx['custom_server_config']** dengan memanfaatkan fungsi pencarian **CTRL W** pada editor nano, lalu ubah nilainya menjadi seperti berikut:
```sh
nginx['custom_gitlab_server_config'] = "location ^~ /.well-known { root /var/www/letsencrypt; }"
```
Simpan konfigurasi lalu konfigurasi ulang GitLab.
```sh
$ sudo gitlab-ctl reconfigure
```

## Mengajukan Permintaan Sertifikat dengan Certbot

```sh
$ sudo certbot certonly --webroot --webroot-path=/var/www/letsencrypt -d domain_anda
```
> Opsi **domain_anda** dapat anda ganti dengan nama domain anda. Misalnya **gitlab.example.com**.

Ikuti instruksi yang ada. Setelah proses selesai, anda dapat menemukan sertifikatnya di direktori `/etc/letsencrypt/live/domain_anda/` dengan hak akses `sudo`.

```sh
$ sudo ls /etc/letsencrypt/live/domain_anda/
```
Empat berkas dengan ekstensi `pem` akan ditampilkan kepada anda, yaitu `cert.pem` `chain.pem` `fullchain.pem` `privkey.pem`. Anda hanya memerkukan `fullchain.pem` dan `privkey.pem` saja untuk kebutuhan konfigurasi SSL/TLS pada GitLab.

## Konfigurasi GitLab Agar Menggunakan Sertifikat Let's Encrypt

```sh
$ sudo nano /etc/gitlab/gitlab.rb
```

Manfaatkan fitur pencarian pada editor nano untuk menemukan `external_url`, ubah dari HTTP menjadi HTTPS.
```sh
external_url 'https://your_domain'
```

Temukan baris `redirect_http_to_https` dan ubah nilainya dari `false` menjadi `true`.
```sh
nginx['redirect_http_to_https'] = true
```

Temukan baris `ssl_certificate` dan `ssl_certificate_key` kemudian ubah nilai atau PATH direktory menjadi seperti berikut ini:
```sh
nginx['ssl_certificate'] = "/etc/letsencrypt/live/domain_anda/fullchain.pem"
nginx['ssl_certificate_key'] = "/etc/letsencrypt/live/domain_anda/privkey.pem"
```
> Ganti **domain_anda** menjadi nama domain, misalnya: **gitlab.example.com**. Gunakan fitur _autocomplete_ dengan mengklik tombol **Tab**.

Konfigurasikan ulang GitLab untuk membaca pembaruan konfigurasi:
```sh
$ sudo gitlab-ctl reconfigure
```

Sekarang, akses gitlab anda melalui peramban web dan nikmatilah :)

# Referensi
[How to Install GitLab on Ubuntu and Debian](https://www.tecmint.com/install-gitlab-on-ubuntu-debian/)

[How To Secure GitLab with Let's Encrypt on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-secure-gitlab-with-let-s-encrypt-on-ubuntu-16-04)